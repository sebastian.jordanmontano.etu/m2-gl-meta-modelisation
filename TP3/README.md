# TP 5

## Quels sont les éléments à traduire ?

- Les classes
- Les attributs
- On aura le nom complet du package comme an attribut dans la table du modèle cible

On ne va pas avoir

- Les méthodes
- Les commentaires

## Quelle va être la traduction d’une classe ?

La traduction d une classe sera une table avec le même nom

## Quelle va être la traduction d’un attribut ?

Un attribut sera une colonne de la table cible

## Comment gérer l’héritage ?

On va utiliser la strategy de `Spring Hibernate` : `Single Table` qui consiste en :

> La strategy single table relacione tous les entités de la structure d héritage dans la même table de la basse de données.

Alors on aura une seul table pour toute la structure d héritage.

## Comment déterminer une clef primaire depuis une classe Java ?

On doit la specifier manuellement quelle sera la clef primaire de la table