/**
 */
package simplejava;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>JJ Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link simplejava.JJClass#getName <em>Name</em>}</li>
 *   <li>{@link simplejava.JJClass#getClasses <em>Classes</em>}</li>
 *   <li>{@link simplejava.JJClass#getInstancevariables <em>Instancevariables</em>}</li>
 *   <li>{@link simplejava.JJClass#getMethods <em>Methods</em>}</li>
 *   <li>{@link simplejava.JJClass#getInterfaces <em>Interfaces</em>}</li>
 * </ul>
 *
 * @see simplejava.SimplejavaPackage#getJJClass()
 * @model
 * @generated
 */
public interface JJClass extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see simplejava.SimplejavaPackage#getJJClass_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link simplejava.JJClass#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Classes</b></em>' containment reference list.
	 * The list contents are of type {@link simplejava.JJClass}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classes</em>' containment reference list.
	 * @see simplejava.SimplejavaPackage#getJJClass_Classes()
	 * @model containment="true"
	 * @generated
	 */
	EList<JJClass> getClasses();

	/**
	 * Returns the value of the '<em><b>Instancevariables</b></em>' containment reference list.
	 * The list contents are of type {@link simplejava.JInstanceVariable}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instancevariables</em>' containment reference list.
	 * @see simplejava.SimplejavaPackage#getJJClass_Instancevariables()
	 * @model containment="true"
	 * @generated
	 */
	EList<JInstanceVariable> getInstancevariables();

	/**
	 * Returns the value of the '<em><b>Methods</b></em>' containment reference list.
	 * The list contents are of type {@link simplejava.JMethod}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Methods</em>' containment reference list.
	 * @see simplejava.SimplejavaPackage#getJJClass_Methods()
	 * @model containment="true"
	 * @generated
	 */
	EList<JMethod> getMethods();

	/**
	 * Returns the value of the '<em><b>Interfaces</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link simplejava.JInterface#getClasses <em>Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interfaces</em>' reference.
	 * @see #setInterfaces(JInterface)
	 * @see simplejava.SimplejavaPackage#getJJClass_Interfaces()
	 * @see simplejava.JInterface#getClasses
	 * @model opposite="classes"
	 * @generated
	 */
	JInterface getInterfaces();

	/**
	 * Sets the value of the '{@link simplejava.JJClass#getInterfaces <em>Interfaces</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interfaces</em>' reference.
	 * @see #getInterfaces()
	 * @generated
	 */
	void setInterfaces(JInterface value);

} // JJClass
