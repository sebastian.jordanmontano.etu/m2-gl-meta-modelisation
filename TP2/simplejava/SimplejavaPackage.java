/**
 */
package simplejava;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see simplejava.SimplejavaFactory
 * @model kind="package"
 * @generated
 */
public interface SimplejavaPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "simplejava";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/simplejava";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "simplejava";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SimplejavaPackage eINSTANCE = simplejava.impl.SimplejavaPackageImpl.init();

	/**
	 * The meta object id for the '{@link simplejava.impl.JPackageImpl <em>JPackage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see simplejava.impl.JPackageImpl
	 * @see simplejava.impl.SimplejavaPackageImpl#getJPackage()
	 * @generated
	 */
	int JPACKAGE = 0;

	/**
	 * The feature id for the '<em><b>Classes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JPACKAGE__CLASSES = 0;

	/**
	 * The feature id for the '<em><b>Packages</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JPACKAGE__PACKAGES = 1;

	/**
	 * The feature id for the '<em><b>Interfaces</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JPACKAGE__INTERFACES = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JPACKAGE__NAME = 3;

	/**
	 * The number of structural features of the '<em>JPackage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JPACKAGE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>JPackage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JPACKAGE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link simplejava.impl.JMethodImpl <em>JMethod</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see simplejava.impl.JMethodImpl
	 * @see simplejava.impl.SimplejavaPackageImpl#getJMethod()
	 * @generated
	 */
	int JMETHOD = 1;

	/**
	 * The feature id for the '<em><b>Temporaryvariables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JMETHOD__TEMPORARYVARIABLES = 0;

	/**
	 * The feature id for the '<em><b>Invokes methods</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JMETHOD__INVOKES_METHODS = 1;

	/**
	 * The feature id for the '<em><b>Instancevariables</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JMETHOD__INSTANCEVARIABLES = 2;

	/**
	 * The feature id for the '<em><b>Statements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JMETHOD__STATEMENTS = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JMETHOD__NAME = 4;

	/**
	 * The number of structural features of the '<em>JMethod</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JMETHOD_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>JMethod</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JMETHOD_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link simplejava.impl.JInterfaceImpl <em>JInterface</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see simplejava.impl.JInterfaceImpl
	 * @see simplejava.impl.SimplejavaPackageImpl#getJInterface()
	 * @generated
	 */
	int JINTERFACE = 2;

	/**
	 * The feature id for the '<em><b>Interfaces</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JINTERFACE__INTERFACES = 0;

	/**
	 * The feature id for the '<em><b>Methods</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JINTERFACE__METHODS = 1;

	/**
	 * The feature id for the '<em><b>Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JINTERFACE__CLASSES = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JINTERFACE__NAME = 3;

	/**
	 * The number of structural features of the '<em>JInterface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JINTERFACE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>JInterface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JINTERFACE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link simplejava.impl.JInstanceVariableImpl <em>JInstance Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see simplejava.impl.JInstanceVariableImpl
	 * @see simplejava.impl.SimplejavaPackageImpl#getJInstanceVariable()
	 * @generated
	 */
	int JINSTANCE_VARIABLE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JINSTANCE_VARIABLE__NAME = 0;

	/**
	 * The number of structural features of the '<em>JInstance Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JINSTANCE_VARIABLE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>JInstance Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JINSTANCE_VARIABLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link simplejava.impl.JTemporaryVariableImpl <em>JTemporary Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see simplejava.impl.JTemporaryVariableImpl
	 * @see simplejava.impl.SimplejavaPackageImpl#getJTemporaryVariable()
	 * @generated
	 */
	int JTEMPORARY_VARIABLE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JTEMPORARY_VARIABLE__NAME = 0;

	/**
	 * The number of structural features of the '<em>JTemporary Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JTEMPORARY_VARIABLE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>JTemporary Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JTEMPORARY_VARIABLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link simplejava.impl.JStatementImpl <em>JStatement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see simplejava.impl.JStatementImpl
	 * @see simplejava.impl.SimplejavaPackageImpl#getJStatement()
	 * @generated
	 */
	int JSTATEMENT = 5;

	/**
	 * The number of structural features of the '<em>JStatement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSTATEMENT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>JStatement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSTATEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link simplejava.impl.JJClassImpl <em>JJ Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see simplejava.impl.JJClassImpl
	 * @see simplejava.impl.SimplejavaPackageImpl#getJJClass()
	 * @generated
	 */
	int JJ_CLASS = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JJ_CLASS__NAME = 0;

	/**
	 * The feature id for the '<em><b>Classes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JJ_CLASS__CLASSES = 1;

	/**
	 * The feature id for the '<em><b>Instancevariables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JJ_CLASS__INSTANCEVARIABLES = 2;

	/**
	 * The feature id for the '<em><b>Methods</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JJ_CLASS__METHODS = 3;

	/**
	 * The feature id for the '<em><b>Interfaces</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JJ_CLASS__INTERFACES = 4;

	/**
	 * The number of structural features of the '<em>JJ Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JJ_CLASS_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>JJ Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JJ_CLASS_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link simplejava.JPackage <em>JPackage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>JPackage</em>'.
	 * @see simplejava.JPackage
	 * @generated
	 */
	EClass getJPackage();

	/**
	 * Returns the meta object for the containment reference list '{@link simplejava.JPackage#getClasses <em>Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Classes</em>'.
	 * @see simplejava.JPackage#getClasses()
	 * @see #getJPackage()
	 * @generated
	 */
	EReference getJPackage_Classes();

	/**
	 * Returns the meta object for the containment reference list '{@link simplejava.JPackage#getPackages <em>Packages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Packages</em>'.
	 * @see simplejava.JPackage#getPackages()
	 * @see #getJPackage()
	 * @generated
	 */
	EReference getJPackage_Packages();

	/**
	 * Returns the meta object for the containment reference list '{@link simplejava.JPackage#getInterfaces <em>Interfaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Interfaces</em>'.
	 * @see simplejava.JPackage#getInterfaces()
	 * @see #getJPackage()
	 * @generated
	 */
	EReference getJPackage_Interfaces();

	/**
	 * Returns the meta object for the attribute '{@link simplejava.JPackage#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see simplejava.JPackage#getName()
	 * @see #getJPackage()
	 * @generated
	 */
	EAttribute getJPackage_Name();

	/**
	 * Returns the meta object for class '{@link simplejava.JMethod <em>JMethod</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>JMethod</em>'.
	 * @see simplejava.JMethod
	 * @generated
	 */
	EClass getJMethod();

	/**
	 * Returns the meta object for the containment reference list '{@link simplejava.JMethod#getTemporaryvariables <em>Temporaryvariables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Temporaryvariables</em>'.
	 * @see simplejava.JMethod#getTemporaryvariables()
	 * @see #getJMethod()
	 * @generated
	 */
	EReference getJMethod_Temporaryvariables();

	/**
	 * Returns the meta object for the reference list '{@link simplejava.JMethod#getInvokes_methods <em>Invokes methods</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Invokes methods</em>'.
	 * @see simplejava.JMethod#getInvokes_methods()
	 * @see #getJMethod()
	 * @generated
	 */
	EReference getJMethod_Invokes_methods();

	/**
	 * Returns the meta object for the reference list '{@link simplejava.JMethod#getInstancevariables <em>Instancevariables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Instancevariables</em>'.
	 * @see simplejava.JMethod#getInstancevariables()
	 * @see #getJMethod()
	 * @generated
	 */
	EReference getJMethod_Instancevariables();

	/**
	 * Returns the meta object for the containment reference list '{@link simplejava.JMethod#getStatements <em>Statements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statements</em>'.
	 * @see simplejava.JMethod#getStatements()
	 * @see #getJMethod()
	 * @generated
	 */
	EReference getJMethod_Statements();

	/**
	 * Returns the meta object for the attribute '{@link simplejava.JMethod#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see simplejava.JMethod#getName()
	 * @see #getJMethod()
	 * @generated
	 */
	EAttribute getJMethod_Name();

	/**
	 * Returns the meta object for class '{@link simplejava.JInterface <em>JInterface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>JInterface</em>'.
	 * @see simplejava.JInterface
	 * @generated
	 */
	EClass getJInterface();

	/**
	 * Returns the meta object for the containment reference list '{@link simplejava.JInterface#getInterfaces <em>Interfaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Interfaces</em>'.
	 * @see simplejava.JInterface#getInterfaces()
	 * @see #getJInterface()
	 * @generated
	 */
	EReference getJInterface_Interfaces();

	/**
	 * Returns the meta object for the containment reference list '{@link simplejava.JInterface#getMethods <em>Methods</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Methods</em>'.
	 * @see simplejava.JInterface#getMethods()
	 * @see #getJInterface()
	 * @generated
	 */
	EReference getJInterface_Methods();

	/**
	 * Returns the meta object for the reference list '{@link simplejava.JInterface#getClasses <em>Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Classes</em>'.
	 * @see simplejava.JInterface#getClasses()
	 * @see #getJInterface()
	 * @generated
	 */
	EReference getJInterface_Classes();

	/**
	 * Returns the meta object for the attribute '{@link simplejava.JInterface#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see simplejava.JInterface#getName()
	 * @see #getJInterface()
	 * @generated
	 */
	EAttribute getJInterface_Name();

	/**
	 * Returns the meta object for class '{@link simplejava.JInstanceVariable <em>JInstance Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>JInstance Variable</em>'.
	 * @see simplejava.JInstanceVariable
	 * @generated
	 */
	EClass getJInstanceVariable();

	/**
	 * Returns the meta object for the attribute '{@link simplejava.JInstanceVariable#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see simplejava.JInstanceVariable#getName()
	 * @see #getJInstanceVariable()
	 * @generated
	 */
	EAttribute getJInstanceVariable_Name();

	/**
	 * Returns the meta object for class '{@link simplejava.JTemporaryVariable <em>JTemporary Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>JTemporary Variable</em>'.
	 * @see simplejava.JTemporaryVariable
	 * @generated
	 */
	EClass getJTemporaryVariable();

	/**
	 * Returns the meta object for the attribute '{@link simplejava.JTemporaryVariable#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see simplejava.JTemporaryVariable#getName()
	 * @see #getJTemporaryVariable()
	 * @generated
	 */
	EAttribute getJTemporaryVariable_Name();

	/**
	 * Returns the meta object for class '{@link simplejava.JStatement <em>JStatement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>JStatement</em>'.
	 * @see simplejava.JStatement
	 * @generated
	 */
	EClass getJStatement();

	/**
	 * Returns the meta object for class '{@link simplejava.JJClass <em>JJ Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>JJ Class</em>'.
	 * @see simplejava.JJClass
	 * @generated
	 */
	EClass getJJClass();

	/**
	 * Returns the meta object for the attribute '{@link simplejava.JJClass#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see simplejava.JJClass#getName()
	 * @see #getJJClass()
	 * @generated
	 */
	EAttribute getJJClass_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link simplejava.JJClass#getClasses <em>Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Classes</em>'.
	 * @see simplejava.JJClass#getClasses()
	 * @see #getJJClass()
	 * @generated
	 */
	EReference getJJClass_Classes();

	/**
	 * Returns the meta object for the containment reference list '{@link simplejava.JJClass#getInstancevariables <em>Instancevariables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Instancevariables</em>'.
	 * @see simplejava.JJClass#getInstancevariables()
	 * @see #getJJClass()
	 * @generated
	 */
	EReference getJJClass_Instancevariables();

	/**
	 * Returns the meta object for the containment reference list '{@link simplejava.JJClass#getMethods <em>Methods</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Methods</em>'.
	 * @see simplejava.JJClass#getMethods()
	 * @see #getJJClass()
	 * @generated
	 */
	EReference getJJClass_Methods();

	/**
	 * Returns the meta object for the reference '{@link simplejava.JJClass#getInterfaces <em>Interfaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Interfaces</em>'.
	 * @see simplejava.JJClass#getInterfaces()
	 * @see #getJJClass()
	 * @generated
	 */
	EReference getJJClass_Interfaces();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SimplejavaFactory getSimplejavaFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link simplejava.impl.JPackageImpl <em>JPackage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see simplejava.impl.JPackageImpl
		 * @see simplejava.impl.SimplejavaPackageImpl#getJPackage()
		 * @generated
		 */
		EClass JPACKAGE = eINSTANCE.getJPackage();

		/**
		 * The meta object literal for the '<em><b>Classes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JPACKAGE__CLASSES = eINSTANCE.getJPackage_Classes();

		/**
		 * The meta object literal for the '<em><b>Packages</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JPACKAGE__PACKAGES = eINSTANCE.getJPackage_Packages();

		/**
		 * The meta object literal for the '<em><b>Interfaces</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JPACKAGE__INTERFACES = eINSTANCE.getJPackage_Interfaces();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JPACKAGE__NAME = eINSTANCE.getJPackage_Name();

		/**
		 * The meta object literal for the '{@link simplejava.impl.JMethodImpl <em>JMethod</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see simplejava.impl.JMethodImpl
		 * @see simplejava.impl.SimplejavaPackageImpl#getJMethod()
		 * @generated
		 */
		EClass JMETHOD = eINSTANCE.getJMethod();

		/**
		 * The meta object literal for the '<em><b>Temporaryvariables</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JMETHOD__TEMPORARYVARIABLES = eINSTANCE.getJMethod_Temporaryvariables();

		/**
		 * The meta object literal for the '<em><b>Invokes methods</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JMETHOD__INVOKES_METHODS = eINSTANCE.getJMethod_Invokes_methods();

		/**
		 * The meta object literal for the '<em><b>Instancevariables</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JMETHOD__INSTANCEVARIABLES = eINSTANCE.getJMethod_Instancevariables();

		/**
		 * The meta object literal for the '<em><b>Statements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JMETHOD__STATEMENTS = eINSTANCE.getJMethod_Statements();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JMETHOD__NAME = eINSTANCE.getJMethod_Name();

		/**
		 * The meta object literal for the '{@link simplejava.impl.JInterfaceImpl <em>JInterface</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see simplejava.impl.JInterfaceImpl
		 * @see simplejava.impl.SimplejavaPackageImpl#getJInterface()
		 * @generated
		 */
		EClass JINTERFACE = eINSTANCE.getJInterface();

		/**
		 * The meta object literal for the '<em><b>Interfaces</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JINTERFACE__INTERFACES = eINSTANCE.getJInterface_Interfaces();

		/**
		 * The meta object literal for the '<em><b>Methods</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JINTERFACE__METHODS = eINSTANCE.getJInterface_Methods();

		/**
		 * The meta object literal for the '<em><b>Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JINTERFACE__CLASSES = eINSTANCE.getJInterface_Classes();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JINTERFACE__NAME = eINSTANCE.getJInterface_Name();

		/**
		 * The meta object literal for the '{@link simplejava.impl.JInstanceVariableImpl <em>JInstance Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see simplejava.impl.JInstanceVariableImpl
		 * @see simplejava.impl.SimplejavaPackageImpl#getJInstanceVariable()
		 * @generated
		 */
		EClass JINSTANCE_VARIABLE = eINSTANCE.getJInstanceVariable();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JINSTANCE_VARIABLE__NAME = eINSTANCE.getJInstanceVariable_Name();

		/**
		 * The meta object literal for the '{@link simplejava.impl.JTemporaryVariableImpl <em>JTemporary Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see simplejava.impl.JTemporaryVariableImpl
		 * @see simplejava.impl.SimplejavaPackageImpl#getJTemporaryVariable()
		 * @generated
		 */
		EClass JTEMPORARY_VARIABLE = eINSTANCE.getJTemporaryVariable();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JTEMPORARY_VARIABLE__NAME = eINSTANCE.getJTemporaryVariable_Name();

		/**
		 * The meta object literal for the '{@link simplejava.impl.JStatementImpl <em>JStatement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see simplejava.impl.JStatementImpl
		 * @see simplejava.impl.SimplejavaPackageImpl#getJStatement()
		 * @generated
		 */
		EClass JSTATEMENT = eINSTANCE.getJStatement();

		/**
		 * The meta object literal for the '{@link simplejava.impl.JJClassImpl <em>JJ Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see simplejava.impl.JJClassImpl
		 * @see simplejava.impl.SimplejavaPackageImpl#getJJClass()
		 * @generated
		 */
		EClass JJ_CLASS = eINSTANCE.getJJClass();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JJ_CLASS__NAME = eINSTANCE.getJJClass_Name();

		/**
		 * The meta object literal for the '<em><b>Classes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JJ_CLASS__CLASSES = eINSTANCE.getJJClass_Classes();

		/**
		 * The meta object literal for the '<em><b>Instancevariables</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JJ_CLASS__INSTANCEVARIABLES = eINSTANCE.getJJClass_Instancevariables();

		/**
		 * The meta object literal for the '<em><b>Methods</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JJ_CLASS__METHODS = eINSTANCE.getJJClass_Methods();

		/**
		 * The meta object literal for the '<em><b>Interfaces</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JJ_CLASS__INTERFACES = eINSTANCE.getJJClass_Interfaces();

	}

} //SimplejavaPackage
