/**
 */
package simplejava;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>JInterface</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link simplejava.JInterface#getInterfaces <em>Interfaces</em>}</li>
 *   <li>{@link simplejava.JInterface#getMethods <em>Methods</em>}</li>
 *   <li>{@link simplejava.JInterface#getClasses <em>Classes</em>}</li>
 *   <li>{@link simplejava.JInterface#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see simplejava.SimplejavaPackage#getJInterface()
 * @model
 * @generated
 */
public interface JInterface extends EObject {
	/**
	 * Returns the value of the '<em><b>Interfaces</b></em>' containment reference list.
	 * The list contents are of type {@link simplejava.JInterface}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interfaces</em>' containment reference list.
	 * @see simplejava.SimplejavaPackage#getJInterface_Interfaces()
	 * @model containment="true"
	 * @generated
	 */
	EList<JInterface> getInterfaces();

	/**
	 * Returns the value of the '<em><b>Methods</b></em>' containment reference list.
	 * The list contents are of type {@link simplejava.JMethod}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Methods</em>' containment reference list.
	 * @see simplejava.SimplejavaPackage#getJInterface_Methods()
	 * @model containment="true"
	 * @generated
	 */
	EList<JMethod> getMethods();

	/**
	 * Returns the value of the '<em><b>Classes</b></em>' reference list.
	 * The list contents are of type {@link simplejava.JJClass}.
	 * It is bidirectional and its opposite is '{@link simplejava.JJClass#getInterfaces <em>Interfaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classes</em>' reference list.
	 * @see simplejava.SimplejavaPackage#getJInterface_Classes()
	 * @see simplejava.JJClass#getInterfaces
	 * @model opposite="interfaces"
	 * @generated
	 */
	EList<JJClass> getClasses();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see simplejava.SimplejavaPackage#getJInterface_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link simplejava.JInterface#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // JInterface
