/**
 */
package simplejava.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import simplejava.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SimplejavaFactoryImpl extends EFactoryImpl implements SimplejavaFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SimplejavaFactory init() {
		try {
			SimplejavaFactory theSimplejavaFactory = (SimplejavaFactory)EPackage.Registry.INSTANCE.getEFactory(SimplejavaPackage.eNS_URI);
			if (theSimplejavaFactory != null) {
				return theSimplejavaFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SimplejavaFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimplejavaFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case SimplejavaPackage.JPACKAGE: return createJPackage();
			case SimplejavaPackage.JMETHOD: return createJMethod();
			case SimplejavaPackage.JINTERFACE: return createJInterface();
			case SimplejavaPackage.JINSTANCE_VARIABLE: return createJInstanceVariable();
			case SimplejavaPackage.JTEMPORARY_VARIABLE: return createJTemporaryVariable();
			case SimplejavaPackage.JSTATEMENT: return createJStatement();
			case SimplejavaPackage.JJ_CLASS: return createJJClass();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JPackage createJPackage() {
		JPackageImpl jPackage = new JPackageImpl();
		return jPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JMethod createJMethod() {
		JMethodImpl jMethod = new JMethodImpl();
		return jMethod;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JInterface createJInterface() {
		JInterfaceImpl jInterface = new JInterfaceImpl();
		return jInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JInstanceVariable createJInstanceVariable() {
		JInstanceVariableImpl jInstanceVariable = new JInstanceVariableImpl();
		return jInstanceVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JTemporaryVariable createJTemporaryVariable() {
		JTemporaryVariableImpl jTemporaryVariable = new JTemporaryVariableImpl();
		return jTemporaryVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JStatement createJStatement() {
		JStatementImpl jStatement = new JStatementImpl();
		return jStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JJClass createJJClass() {
		JJClassImpl jjClass = new JJClassImpl();
		return jjClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimplejavaPackage getSimplejavaPackage() {
		return (SimplejavaPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SimplejavaPackage getPackage() {
		return SimplejavaPackage.eINSTANCE;
	}

} //SimplejavaFactoryImpl
