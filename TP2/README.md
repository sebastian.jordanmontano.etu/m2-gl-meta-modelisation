# M2-GL-Meta-Modelisation

En suivant les instructions du tp, le modèle est correctement créé: 
![](./img/project_created.png)

La class `ModelManipulator` est une classe qui a les méthodes pour faire le tp.

> Écrivez une fonction qui prend en paramètre l’équivalent d’un `JavaModel` et qui retourne la liste de toutes les classes d’un de vos modèles `simplejava`

```java
	private static EList<JJClass> getClasses(JPackage aModel) {
		return aModel.getClasses();
	}
```

> Écrivez une fonction qui affiche à l’écran le nom de tous les attributs (equivalent `JAttribute` contenues par les `JClass`) de votre modèle java.

```java
	private static void showAttributes(JPackage aModel) {
		EList<JJClass> classes = getClasses(aModel);

		for (int i = 0; i < classes.size(); i++) {
			JJClass aClass = classes.get(i);

			EList<JInstanceVariable> attributes = aClass.getInstancevariables();
			for (int j = 0; j < attributes.size(); j++) {
				System.out.println("Attributes for Class " + aClass.getName());
				System.out.println(attributes.get(j).getName());
			}

			System.out.println();
		}
	}
```

> Écrivez une fonction qui retourne la profondeur d’héritage d’une JClass classe en particulier (par exemple, en considérant que l’on a une instance de class JObject nommé Object dans notre méta-modèle, 0 sera retourné pour cette instance de JClass, 1 pour les classes qui héritent de Object, 2 pour les classes qui héritent des précédentes et ainsi de suite

```java
	private static int deepthOfAClass(JJClass aClass) {
		if (!(aClass.eContainer() instanceof JJClass)) {
			return 0;
		}
		return deepthOfAClass((JJClass) aClass.eContainer()) + 1;
	}
```

> Écrivez une fonction qui retourne le nom pleinement qualifié d’une instance de JClass (le nom de tous ses packages contenant séparés par un ‘.‘ suivi du nom de la classe)

```java
	private static String getClassNameWithPackages(JJClass aClass) {
		String name = "";
		JPackage aPackage = (JPackage) aClass.eContainer();
		name = name + aPackage.getName() + ".";
		while (aPackage.eContainer() instanceof JPackage) {
			aPackage = (JPackage) aPackage.eContainer();
			name = name + aPackage.getName() + ".";
		}
		name = name + aClass.getName();
		return name;
	}
```

> Écrivez une fonction qui retourne le nombre de fichiers de votre modèle



> Écrivez une fonction qui retourne le poids de tous les fichiers contenus dans un répertoire

```java
private static int getNumberOfFilesInsideDirectory(File aFile) {
		File[] files = aFile.listFiles();
		int count = 0;
		for (File f : files)
			if (f.isDirectory())
				count += getNumberOfFilesInsideDirectory(f);
			else
				count++;

		return count;
	}
```

> Écrivez une fonction qui retourne le poids de tous les fichiers contenus dans le modèle

```java
private static int countAllFilesSize(File aFile) {
		File[] files = aFile.listFiles();
		int sizeInBytes = 0;
		for (File f : files)
			if (f.isDirectory())
				sizeInBytes += getNumberOfFilesInsideDirectory(f);
			else
				sizeInBytes += f.length();

		return sizeInBytes;
	}
```