/**
 */
package simplejava;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see simplejava.SimplejavaPackage
 * @generated
 */
public interface SimplejavaFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SimplejavaFactory eINSTANCE = simplejava.impl.SimplejavaFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>JPackage</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>JPackage</em>'.
	 * @generated
	 */
	JPackage createJPackage();

	/**
	 * Returns a new object of class '<em>JMethod</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>JMethod</em>'.
	 * @generated
	 */
	JMethod createJMethod();

	/**
	 * Returns a new object of class '<em>JInterface</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>JInterface</em>'.
	 * @generated
	 */
	JInterface createJInterface();

	/**
	 * Returns a new object of class '<em>JInstance Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>JInstance Variable</em>'.
	 * @generated
	 */
	JInstanceVariable createJInstanceVariable();

	/**
	 * Returns a new object of class '<em>JTemporary Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>JTemporary Variable</em>'.
	 * @generated
	 */
	JTemporaryVariable createJTemporaryVariable();

	/**
	 * Returns a new object of class '<em>JStatement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>JStatement</em>'.
	 * @generated
	 */
	JStatement createJStatement();

	/**
	 * Returns a new object of class '<em>JJ Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>JJ Class</em>'.
	 * @generated
	 */
	JJClass createJJClass();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	SimplejavaPackage getSimplejavaPackage();

} //SimplejavaFactory
