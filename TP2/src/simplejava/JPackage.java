/**
 */
package simplejava;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>JPackage</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link simplejava.JPackage#getClasses <em>Classes</em>}</li>
 *   <li>{@link simplejava.JPackage#getPackages <em>Packages</em>}</li>
 *   <li>{@link simplejava.JPackage#getInterfaces <em>Interfaces</em>}</li>
 *   <li>{@link simplejava.JPackage#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see simplejava.SimplejavaPackage#getJPackage()
 * @model
 * @generated
 */
public interface JPackage extends EObject {
	/**
	 * Returns the value of the '<em><b>Classes</b></em>' containment reference list.
	 * The list contents are of type {@link simplejava.JJClass}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classes</em>' containment reference list.
	 * @see simplejava.SimplejavaPackage#getJPackage_Classes()
	 * @model containment="true"
	 * @generated
	 */
	EList<JJClass> getClasses();

	/**
	 * Returns the value of the '<em><b>Packages</b></em>' containment reference list.
	 * The list contents are of type {@link simplejava.JPackage}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Packages</em>' containment reference list.
	 * @see simplejava.SimplejavaPackage#getJPackage_Packages()
	 * @model containment="true"
	 * @generated
	 */
	EList<JPackage> getPackages();

	/**
	 * Returns the value of the '<em><b>Interfaces</b></em>' containment reference list.
	 * The list contents are of type {@link simplejava.JInterface}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interfaces</em>' containment reference list.
	 * @see simplejava.SimplejavaPackage#getJPackage_Interfaces()
	 * @model containment="true"
	 * @generated
	 */
	EList<JInterface> getInterfaces();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see simplejava.SimplejavaPackage#getJPackage_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link simplejava.JPackage#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // JPackage
