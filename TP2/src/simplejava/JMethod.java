/**
 */
package simplejava;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>JMethod</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link simplejava.JMethod#getTemporaryvariables <em>Temporaryvariables</em>}</li>
 *   <li>{@link simplejava.JMethod#getInvokes_methods <em>Invokes methods</em>}</li>
 *   <li>{@link simplejava.JMethod#getInstancevariables <em>Instancevariables</em>}</li>
 *   <li>{@link simplejava.JMethod#getStatements <em>Statements</em>}</li>
 *   <li>{@link simplejava.JMethod#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see simplejava.SimplejavaPackage#getJMethod()
 * @model
 * @generated
 */
public interface JMethod extends EObject {
	/**
	 * Returns the value of the '<em><b>Temporaryvariables</b></em>' containment reference list.
	 * The list contents are of type {@link simplejava.JTemporaryVariable}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Temporaryvariables</em>' containment reference list.
	 * @see simplejava.SimplejavaPackage#getJMethod_Temporaryvariables()
	 * @model containment="true"
	 * @generated
	 */
	EList<JTemporaryVariable> getTemporaryvariables();

	/**
	 * Returns the value of the '<em><b>Invokes methods</b></em>' reference list.
	 * The list contents are of type {@link simplejava.JMethod}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Invokes methods</em>' reference list.
	 * @see simplejava.SimplejavaPackage#getJMethod_Invokes_methods()
	 * @model
	 * @generated
	 */
	EList<JMethod> getInvokes_methods();

	/**
	 * Returns the value of the '<em><b>Instancevariables</b></em>' reference list.
	 * The list contents are of type {@link simplejava.JInstanceVariable}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instancevariables</em>' reference list.
	 * @see simplejava.SimplejavaPackage#getJMethod_Instancevariables()
	 * @model
	 * @generated
	 */
	EList<JInstanceVariable> getInstancevariables();

	/**
	 * Returns the value of the '<em><b>Statements</b></em>' containment reference list.
	 * The list contents are of type {@link simplejava.JStatement}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Statements</em>' containment reference list.
	 * @see simplejava.SimplejavaPackage#getJMethod_Statements()
	 * @model containment="true"
	 * @generated
	 */
	EList<JStatement> getStatements();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see simplejava.SimplejavaPackage#getJMethod_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link simplejava.JMethod#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // JMethod
