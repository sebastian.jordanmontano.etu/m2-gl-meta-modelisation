/**
 */
package simplejava.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import simplejava.JInstanceVariable;
import simplejava.JMethod;
import simplejava.JStatement;
import simplejava.JTemporaryVariable;
import simplejava.SimplejavaPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>JMethod</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link simplejava.impl.JMethodImpl#getTemporaryvariables <em>Temporaryvariables</em>}</li>
 *   <li>{@link simplejava.impl.JMethodImpl#getInvokes_methods <em>Invokes methods</em>}</li>
 *   <li>{@link simplejava.impl.JMethodImpl#getInstancevariables <em>Instancevariables</em>}</li>
 *   <li>{@link simplejava.impl.JMethodImpl#getStatements <em>Statements</em>}</li>
 *   <li>{@link simplejava.impl.JMethodImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JMethodImpl extends MinimalEObjectImpl.Container implements JMethod {
	/**
	 * The cached value of the '{@link #getTemporaryvariables() <em>Temporaryvariables</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemporaryvariables()
	 * @generated
	 * @ordered
	 */
	protected EList<JTemporaryVariable> temporaryvariables;

	/**
	 * The cached value of the '{@link #getInvokes_methods() <em>Invokes methods</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInvokes_methods()
	 * @generated
	 * @ordered
	 */
	protected EList<JMethod> invokes_methods;

	/**
	 * The cached value of the '{@link #getInstancevariables() <em>Instancevariables</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstancevariables()
	 * @generated
	 * @ordered
	 */
	protected EList<JInstanceVariable> instancevariables;

	/**
	 * The cached value of the '{@link #getStatements() <em>Statements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatements()
	 * @generated
	 * @ordered
	 */
	protected EList<JStatement> statements;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JMethodImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SimplejavaPackage.Literals.JMETHOD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<JTemporaryVariable> getTemporaryvariables() {
		if (temporaryvariables == null) {
			temporaryvariables = new EObjectContainmentEList<JTemporaryVariable>(JTemporaryVariable.class, this, SimplejavaPackage.JMETHOD__TEMPORARYVARIABLES);
		}
		return temporaryvariables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<JMethod> getInvokes_methods() {
		if (invokes_methods == null) {
			invokes_methods = new EObjectResolvingEList<JMethod>(JMethod.class, this, SimplejavaPackage.JMETHOD__INVOKES_METHODS);
		}
		return invokes_methods;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<JInstanceVariable> getInstancevariables() {
		if (instancevariables == null) {
			instancevariables = new EObjectResolvingEList<JInstanceVariable>(JInstanceVariable.class, this, SimplejavaPackage.JMETHOD__INSTANCEVARIABLES);
		}
		return instancevariables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<JStatement> getStatements() {
		if (statements == null) {
			statements = new EObjectContainmentEList<JStatement>(JStatement.class, this, SimplejavaPackage.JMETHOD__STATEMENTS);
		}
		return statements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SimplejavaPackage.JMETHOD__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SimplejavaPackage.JMETHOD__TEMPORARYVARIABLES:
				return ((InternalEList<?>)getTemporaryvariables()).basicRemove(otherEnd, msgs);
			case SimplejavaPackage.JMETHOD__STATEMENTS:
				return ((InternalEList<?>)getStatements()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SimplejavaPackage.JMETHOD__TEMPORARYVARIABLES:
				return getTemporaryvariables();
			case SimplejavaPackage.JMETHOD__INVOKES_METHODS:
				return getInvokes_methods();
			case SimplejavaPackage.JMETHOD__INSTANCEVARIABLES:
				return getInstancevariables();
			case SimplejavaPackage.JMETHOD__STATEMENTS:
				return getStatements();
			case SimplejavaPackage.JMETHOD__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SimplejavaPackage.JMETHOD__TEMPORARYVARIABLES:
				getTemporaryvariables().clear();
				getTemporaryvariables().addAll((Collection<? extends JTemporaryVariable>)newValue);
				return;
			case SimplejavaPackage.JMETHOD__INVOKES_METHODS:
				getInvokes_methods().clear();
				getInvokes_methods().addAll((Collection<? extends JMethod>)newValue);
				return;
			case SimplejavaPackage.JMETHOD__INSTANCEVARIABLES:
				getInstancevariables().clear();
				getInstancevariables().addAll((Collection<? extends JInstanceVariable>)newValue);
				return;
			case SimplejavaPackage.JMETHOD__STATEMENTS:
				getStatements().clear();
				getStatements().addAll((Collection<? extends JStatement>)newValue);
				return;
			case SimplejavaPackage.JMETHOD__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SimplejavaPackage.JMETHOD__TEMPORARYVARIABLES:
				getTemporaryvariables().clear();
				return;
			case SimplejavaPackage.JMETHOD__INVOKES_METHODS:
				getInvokes_methods().clear();
				return;
			case SimplejavaPackage.JMETHOD__INSTANCEVARIABLES:
				getInstancevariables().clear();
				return;
			case SimplejavaPackage.JMETHOD__STATEMENTS:
				getStatements().clear();
				return;
			case SimplejavaPackage.JMETHOD__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SimplejavaPackage.JMETHOD__TEMPORARYVARIABLES:
				return temporaryvariables != null && !temporaryvariables.isEmpty();
			case SimplejavaPackage.JMETHOD__INVOKES_METHODS:
				return invokes_methods != null && !invokes_methods.isEmpty();
			case SimplejavaPackage.JMETHOD__INSTANCEVARIABLES:
				return instancevariables != null && !instancevariables.isEmpty();
			case SimplejavaPackage.JMETHOD__STATEMENTS:
				return statements != null && !statements.isEmpty();
			case SimplejavaPackage.JMETHOD__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //JMethodImpl
