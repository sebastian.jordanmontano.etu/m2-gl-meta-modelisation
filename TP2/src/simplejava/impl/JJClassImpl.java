/**
 */
package simplejava.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import simplejava.JInstanceVariable;
import simplejava.JInterface;
import simplejava.JJClass;
import simplejava.JMethod;
import simplejava.SimplejavaPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>JJ Class</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link simplejava.impl.JJClassImpl#getName <em>Name</em>}</li>
 *   <li>{@link simplejava.impl.JJClassImpl#getClasses <em>Classes</em>}</li>
 *   <li>{@link simplejava.impl.JJClassImpl#getInstancevariables <em>Instancevariables</em>}</li>
 *   <li>{@link simplejava.impl.JJClassImpl#getMethods <em>Methods</em>}</li>
 *   <li>{@link simplejava.impl.JJClassImpl#getInterfaces <em>Interfaces</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JJClassImpl extends MinimalEObjectImpl.Container implements JJClass {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getClasses() <em>Classes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClasses()
	 * @generated
	 * @ordered
	 */
	protected EList<JJClass> classes;

	/**
	 * The cached value of the '{@link #getInstancevariables() <em>Instancevariables</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstancevariables()
	 * @generated
	 * @ordered
	 */
	protected EList<JInstanceVariable> instancevariables;

	/**
	 * The cached value of the '{@link #getMethods() <em>Methods</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMethods()
	 * @generated
	 * @ordered
	 */
	protected EList<JMethod> methods;

	/**
	 * The cached value of the '{@link #getInterfaces() <em>Interfaces</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfaces()
	 * @generated
	 * @ordered
	 */
	protected JInterface interfaces;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JJClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SimplejavaPackage.Literals.JJ_CLASS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SimplejavaPackage.JJ_CLASS__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<JJClass> getClasses() {
		if (classes == null) {
			classes = new EObjectContainmentEList<JJClass>(JJClass.class, this, SimplejavaPackage.JJ_CLASS__CLASSES);
		}
		return classes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<JInstanceVariable> getInstancevariables() {
		if (instancevariables == null) {
			instancevariables = new EObjectContainmentEList<JInstanceVariable>(JInstanceVariable.class, this, SimplejavaPackage.JJ_CLASS__INSTANCEVARIABLES);
		}
		return instancevariables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<JMethod> getMethods() {
		if (methods == null) {
			methods = new EObjectContainmentEList<JMethod>(JMethod.class, this, SimplejavaPackage.JJ_CLASS__METHODS);
		}
		return methods;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JInterface getInterfaces() {
		if (interfaces != null && interfaces.eIsProxy()) {
			InternalEObject oldInterfaces = (InternalEObject)interfaces;
			interfaces = (JInterface)eResolveProxy(oldInterfaces);
			if (interfaces != oldInterfaces) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SimplejavaPackage.JJ_CLASS__INTERFACES, oldInterfaces, interfaces));
			}
		}
		return interfaces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JInterface basicGetInterfaces() {
		return interfaces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInterfaces(JInterface newInterfaces, NotificationChain msgs) {
		JInterface oldInterfaces = interfaces;
		interfaces = newInterfaces;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SimplejavaPackage.JJ_CLASS__INTERFACES, oldInterfaces, newInterfaces);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterfaces(JInterface newInterfaces) {
		if (newInterfaces != interfaces) {
			NotificationChain msgs = null;
			if (interfaces != null)
				msgs = ((InternalEObject)interfaces).eInverseRemove(this, SimplejavaPackage.JINTERFACE__CLASSES, JInterface.class, msgs);
			if (newInterfaces != null)
				msgs = ((InternalEObject)newInterfaces).eInverseAdd(this, SimplejavaPackage.JINTERFACE__CLASSES, JInterface.class, msgs);
			msgs = basicSetInterfaces(newInterfaces, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SimplejavaPackage.JJ_CLASS__INTERFACES, newInterfaces, newInterfaces));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SimplejavaPackage.JJ_CLASS__INTERFACES:
				if (interfaces != null)
					msgs = ((InternalEObject)interfaces).eInverseRemove(this, SimplejavaPackage.JINTERFACE__CLASSES, JInterface.class, msgs);
				return basicSetInterfaces((JInterface)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SimplejavaPackage.JJ_CLASS__CLASSES:
				return ((InternalEList<?>)getClasses()).basicRemove(otherEnd, msgs);
			case SimplejavaPackage.JJ_CLASS__INSTANCEVARIABLES:
				return ((InternalEList<?>)getInstancevariables()).basicRemove(otherEnd, msgs);
			case SimplejavaPackage.JJ_CLASS__METHODS:
				return ((InternalEList<?>)getMethods()).basicRemove(otherEnd, msgs);
			case SimplejavaPackage.JJ_CLASS__INTERFACES:
				return basicSetInterfaces(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SimplejavaPackage.JJ_CLASS__NAME:
				return getName();
			case SimplejavaPackage.JJ_CLASS__CLASSES:
				return getClasses();
			case SimplejavaPackage.JJ_CLASS__INSTANCEVARIABLES:
				return getInstancevariables();
			case SimplejavaPackage.JJ_CLASS__METHODS:
				return getMethods();
			case SimplejavaPackage.JJ_CLASS__INTERFACES:
				if (resolve) return getInterfaces();
				return basicGetInterfaces();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SimplejavaPackage.JJ_CLASS__NAME:
				setName((String)newValue);
				return;
			case SimplejavaPackage.JJ_CLASS__CLASSES:
				getClasses().clear();
				getClasses().addAll((Collection<? extends JJClass>)newValue);
				return;
			case SimplejavaPackage.JJ_CLASS__INSTANCEVARIABLES:
				getInstancevariables().clear();
				getInstancevariables().addAll((Collection<? extends JInstanceVariable>)newValue);
				return;
			case SimplejavaPackage.JJ_CLASS__METHODS:
				getMethods().clear();
				getMethods().addAll((Collection<? extends JMethod>)newValue);
				return;
			case SimplejavaPackage.JJ_CLASS__INTERFACES:
				setInterfaces((JInterface)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SimplejavaPackage.JJ_CLASS__NAME:
				setName(NAME_EDEFAULT);
				return;
			case SimplejavaPackage.JJ_CLASS__CLASSES:
				getClasses().clear();
				return;
			case SimplejavaPackage.JJ_CLASS__INSTANCEVARIABLES:
				getInstancevariables().clear();
				return;
			case SimplejavaPackage.JJ_CLASS__METHODS:
				getMethods().clear();
				return;
			case SimplejavaPackage.JJ_CLASS__INTERFACES:
				setInterfaces((JInterface)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SimplejavaPackage.JJ_CLASS__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case SimplejavaPackage.JJ_CLASS__CLASSES:
				return classes != null && !classes.isEmpty();
			case SimplejavaPackage.JJ_CLASS__INSTANCEVARIABLES:
				return instancevariables != null && !instancevariables.isEmpty();
			case SimplejavaPackage.JJ_CLASS__METHODS:
				return methods != null && !methods.isEmpty();
			case SimplejavaPackage.JJ_CLASS__INTERFACES:
				return interfaces != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //JJClassImpl
