/**
 */
package simplejava;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>JStatement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see simplejava.SimplejavaPackage#getJStatement()
 * @model
 * @generated
 */
public interface JStatement extends EObject {
} // JStatement
