package sebas;

import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import simplejava.JInstanceVariable;
import simplejava.JJClass;
import simplejava.JPackage;
import simplejava.SimplejavaPackage;

public class ModelManipulator {

	private static EList<JJClass> getClasses(JPackage aModel) {
		return aModel.getClasses();
	}

	private static void showAttributes(JPackage aModel) {
		EList<JJClass> classes = getClasses(aModel);

		for (int i = 0; i < classes.size(); i++) {
			JJClass aClass = classes.get(i);

			EList<JInstanceVariable> attributes = aClass.getInstancevariables();
			for (int j = 0; j < attributes.size(); j++) {
				System.out.println("Attributes for Class " + aClass.getName());
				System.out.println(attributes.get(j).getName());
			}

			System.out.println();
		}
	}

	private static int deepthOfAClass(JJClass aClass) {
		if (!(aClass.eContainer() instanceof JJClass)) {
			return 0;
		}
		return deepthOfAClass((JJClass) aClass.eContainer()) + 1;
	}

	private static String getClassNameWithPackages(JJClass aClass) {
		String name = "";
		JPackage aPackage = (JPackage) aClass.eContainer();
		name = name + aPackage.getName() + ".";
		while (aPackage.eContainer() instanceof JPackage) {
			aPackage = (JPackage) aPackage.eContainer();
			name = name + aPackage.getName() + ".";
		}
		name = name + aClass.getName();
		return name;
	}

	private static int getNumberOfFilesInsideDirectory(File aFile) {
		File[] files = aFile.listFiles();
		int count = 0;
		for (File f : files)
			if (f.isDirectory())
				count += getNumberOfFilesInsideDirectory(f);
			else
				count++;

		return count;
	}

	private static int countAllFilesSize(File aFile) {
		File[] files = aFile.listFiles();
		int sizeInBytes = 0;
		for (File f : files)
			if (f.isDirectory())
				sizeInBytes += getNumberOfFilesInsideDirectory(f);
			else
				sizeInBytes += f.length();

		return sizeInBytes;
	}

	public static void main(String[] args) {
		// Dynamiquement 
		// ResourceSet rset = new ResourceSetImpl();
		// rset.getResourceFactoryRegistry().getExtensionToFactoryMap().put("ecore", new XMIResourceFactoryImpl());
		// rset.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
		//rset.getPackageRegistry().put(SimplejavaPackage.eNS_URI, SimplejavaPackage.eINSTANCE);
		// Resource resource = rset.getResource(URI.createFileURI("model/JPackage.xmi"), true);
		// JPackage model = (JPackage) resource.getContents().get(0);
		
		// Statiquement
		ResourceSet rset = new ResourceSetImpl();
		rset.getResourceFactoryRegistry().getExtensionToFactoryMap().put("ecore", new XMIResourceFactoryImpl());
		rset.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
		Resource resource = rset.getResource(URI.createFileURI("model/tP1.ecore"), true);
		EPackage ePack = (EPackage) resource.getContents().get(0);
		
		
		System.out.println(ePack.getClass().getName());
	}

}
