# TP1

_Attention! Au lieu de dire `simplejava` pour le méta-modèle de Java, il dit: `TP1`. Alors, dans la structure, `basededonnees` correspond au méta-modèle d'une base de données et `TP1` correspond au méta-modèle du java._

![](./img/files-structure.png)

Pour charger le projet sur Eclipse, il faut séleccioner le dossier TP1.

## `simplejava` méta-modèle

Le métamodèle qui a été créer pour répresenter le langage de programmation Java:

![](./img/java-metamodel.png)

Après de creér le modèle et de faire sa validation, les instances dynamiques ont ensuite été générées.

![](./img/java-dynamic.png)

On peut voir aussi q'une classe a une interface 

![](./img/class%20with%20instance.png)

## `base de données` méta-modèle

Le métamodèle qui a été créer pour répresenter une base de données:

![](./img/database-metamodel.png)

Ici, on peut voir les instances dynamiques

![](./img/database-dynamic.png)
