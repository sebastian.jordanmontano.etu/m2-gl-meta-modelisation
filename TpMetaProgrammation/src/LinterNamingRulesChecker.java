import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

public class LinterNamingRulesChecker {
    private LinterNamingRules linterNamingRules = new LinterNamingRules();
    private Set<Class<?>> classesToCheck = new HashSet<>();

    public LinterNamingRulesChecker(Set<Class<?>> classesToCheck) {
        this.classesToCheck = classesToCheck;
    }

    public String getErrorMessage(String nameOfTheRule, String classOfMethodName) {
        return "The naming rule: " + nameOfTheRule + " was violated by: " + classOfMethodName;
    }

    public void check() {
        for (Class<?> c : this.classesToCheck) {
            if (!linterNamingRules.namesOfAllClassesMustStartByCapitalLetter(c)) {
                System.out.println(getErrorMessage("namesOfAllClassesMustStartByCapitalLetter", c.getSimpleName()));
            }
            if (!linterNamingRules.namesOfAllClassesShouldNotBeLongerThan20(c)) {
                System.out.println(getErrorMessage("namesOfAllClassesShouldNotBeLongerThan20", c.getSimpleName()));
            }
            if (!linterNamingRules.interfaceNameShouldStartByI(c)) {
                System.out.println(getErrorMessage("interfaceNameShouldStartByI", c.getSimpleName()));
            }
            if (!linterNamingRules.enumNameShouldStartByE(c)) {
                System.out.println(getErrorMessage("enumNameShouldStartByE", c.getSimpleName()));
            }
            if (!linterNamingRules.abstractClassNameShouldStartByA(c)) {
                System.out.println(getErrorMessage("abstractClassNameShouldStartByA", c.getSimpleName()));
            }

            for (Method m : c.getDeclaredMethods()) {
                if (!linterNamingRules.methodNameShouldStartByLowerCase(m)) {
                    System.out.println(
                            getErrorMessage("methodNameShouldStartByLowerCase",
                                    (c.getSimpleName() + ">>" + m.getName())));
                }
            }

            for (Field f : c.getDeclaredFields()) {
                if (!linterNamingRules.attributeNameShouldStartByLowerCase(f)) {
                    System.out.println(
                            getErrorMessage("attributeNameShouldStartByLowerCase",
                                    (c.getSimpleName() + ">>" + f.getName())));
                }
                if (!linterNamingRules.staticAttributeNameShouldStartByUpperCase(f)) {
                    System.out.println(getErrorMessage("staticAttributeNameShouldStartByUpperCase",
                            (c.getSimpleName() + ">>" + f.getName())));
                }
            }

        }
    }
}
