import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class LinterNamingRules {

    private ReflexionHelper reflexionHelper = new ReflexionHelper();

    public Boolean namesOfAllClassesMustStartByCapitalLetter(Class<?> aClass) {
        char firstCharacterOfName = aClass.getSimpleName().charAt(0);
        return Character.isUpperCase(firstCharacterOfName);
    }

    public Boolean namesOfAllClassesShouldNotBeLongerThan20(Class<?> aClass) {
        return aClass.getSimpleName().length() < 20;
    }

    public Boolean interfaceNameShouldStartByI(Class<?> anInterface) {
        if (!reflexionHelper.isClassInterface(anInterface)) {
            return true;
        }
        char firstCharacterOfName = anInterface.getSimpleName().charAt(0);
        return firstCharacterOfName == 'I';
    }

    public Boolean enumNameShouldStartByE(Class<?> anEnum) {
        if (!reflexionHelper.isClassEnum(anEnum)) {
            return true;
        }
        char firstCharacterOfName = anEnum.getSimpleName().charAt(0);
        return firstCharacterOfName == 'E';
    }

    public Boolean abstractClassNameShouldStartByA(Class<?> anAbstractClass) {
        if (!reflexionHelper.isClassAbstract(anAbstractClass)) {
            return true;
        }
        char firstCharacterOfName = anAbstractClass.getSimpleName().charAt(0);
        return firstCharacterOfName == 'A';
    }

    public Boolean methodNameShouldStartByLowerCase(Method aMethod) {
        char firstCharacterOfName = aMethod.getName().charAt(0);
        return Character.isLowerCase(firstCharacterOfName);
    }

    public Boolean attributeNameShouldStartByLowerCase(Field anAttribute) {
        char firstCharacterOfName = anAttribute.getName().charAt(0);
        if (reflexionHelper.isAttributeFinal(anAttribute)) {
            return true;
        } else {
            return Character.isLowerCase(firstCharacterOfName);

        }
    }

    public Boolean staticAttributeNameShouldStartByUpperCase(Field anAttribute) {
        char firstCharacterOfName = anAttribute.getName().charAt(0);
        if (reflexionHelper.isAttributeFinal(anAttribute)) {
            return Character.isUpperCase(firstCharacterOfName);
        } else {
            return true;
        }
    }
}
