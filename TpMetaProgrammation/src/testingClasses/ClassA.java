package testingClasses;

public class ClassA {

    public int anAttribute;
    // This violates a rule
    public int ANAtribute;

    // This does not violate a rule
    public final int ANOTHER_ATTRIBUTE = 5;

    // This violates a rule
    public final int another_ATTRIBUTE = 5;

    public void ThisMethodViolatesARule() {
    }

    public void thisMethodDoesNotViolateARule() {
    }
}
