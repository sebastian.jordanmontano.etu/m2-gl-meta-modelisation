import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

public class LinterStructuralRules {

    private ReflexionHelper reflexionHelper = new ReflexionHelper();

    public Boolean abstractClassShouldHaveAtLeastOneAbstractMethod(Class<?> anAbstractClass) {
        if (!this.reflexionHelper.isClassAbstract(anAbstractClass)) {
            return true;
        }

        Method[] methodsOfTheClass = anAbstractClass.getDeclaredMethods();
        Boolean doesTheClassHasAbstractMethods = false;
        for (Method m : methodsOfTheClass) {
            if (Modifier.isAbstract(m.getModifiers())) {
                doesTheClassHasAbstractMethods = true;
            }
        }
        return doesTheClassHasAbstractMethods;
    }

    public Boolean enumShouldHaveAtLeastTwoTypes(Class<?> anEnum) {
        if (!this.reflexionHelper.isClassEnum(anEnum)) {
            return true;
        }

        Object[] enumConstants = anEnum.getEnumConstants();
        return (enumConstants.length >= 2);
    }

    public Boolean privateOrProtectedClassesShouldNotHavePublic(Class<?> aClass) {
        if (!reflexionHelper.isClassPrivateOrProtected(aClass)) {
            return true;
        }

        Method[] methods = aClass.getDeclaredMethods();
        Field[] fields = aClass.getDeclaredFields();
        Boolean hasPublicMethods = false;
        Boolean hasPublicAttributes = false;
        for (Method m : methods) {
            if (Modifier.isPublic(m.getModifiers())) {
                hasPublicMethods = true;
            }
        }
        for (Field f : fields) {
            if (Modifier.isPublic(f.getModifiers())) {
                hasPublicMethods = true;
            }
        }
        return !(hasPublicMethods || hasPublicAttributes);
    }

    public Boolean allInterfacesShouldBeImplemented(Set<Class<?>> classes) {
        Set<Class<?>> allInterfaces = this.reflexionHelper.getAllInterfaces(classes);
        Set<Class<?>> implementedInterfaces = new HashSet<>();

        for (Class<?> c : classes) {
            for (Class<?> i : c.getInterfaces()) {
                implementedInterfaces.add(i);
            }
        }
        return allInterfaces.size() == implementedInterfaces.size();
    }

    public Boolean packagesShouldHaveAtLeastTwoClasses(Set<Class<?>> classes) {
        Set<Package> allPackages = this.reflexionHelper.getAllPackages(classes);
        Map<String, Integer> dict = new Hashtable<String, Integer>();

        for (Package p : allPackages) {
            dict.put(p.getName(), 0);
        }

        for (Class<?> c : classes) {
            String packageName = c.getPackage().getName();
            int previousValue = (int) dict.get(packageName);
            dict.put(packageName, (previousValue + 1));
        }

        for (int value : dict.values()) {
            // Si un package a moins de 2 classes
            if (value < 2) {
                return false;
            }
        }
        return true;
    }
}