import java.util.HashSet;
import java.util.Set;

import testingClasses.*;

public class App {

    public static String getErrorMessage(String nameOfTheRule, String classOfMethodName) {
        return "The rule: " + nameOfTheRule + " was violated by: " + classOfMethodName;
    }

    public static void main(String[] args) throws Exception {
        Set<Class<?>> classes = new HashSet<>();
        // Adding the classes
        classes.add(abstractClassD.class);
        classes.add(AbstractClassF.class);
        classes.add(ClassA.class);
        classes.add(ClassB.class);
        classes.add(classD.class);
        classes.add(EnumC.class);
        classes.add(EnumI.class);
        classes.add(InterfaceB.class);
        classes.add(interfaceG.class);
        classes.add(SuperLongClassHSuperLongNameThatIsMoreThantwentyCharacters.class);

        // Checking naming rules
        LinterNamingRulesChecker linterNamingRulesChecker = new LinterNamingRulesChecker(classes);
        linterNamingRulesChecker.check();

        // Checking structural rules
        LinterStructuralRulesChecker linterStructuralRulesChecker = new LinterStructuralRulesChecker(classes);
        linterStructuralRulesChecker.check();
    }
}
