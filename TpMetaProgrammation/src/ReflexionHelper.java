import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Set;

public class ReflexionHelper {

    public Boolean isClassAbstract(Class<?> aClass) {
        return Modifier.isAbstract(aClass.getModifiers()) && !isClassInterface(aClass);
    }

    public Boolean isClassInterface(Class<?> aClass) {
        return aClass.isInterface();
    }

    public Boolean isAttributeFinal(Field aField) {
        return Modifier.isFinal(aField.getModifiers());
    }

    public Boolean isClassEnum(Class<?> aClass) {
        return aClass.isEnum();
    }

    public Boolean isClassPrivateOrProtected(Class<?> aClass) {
        return Modifier.isPrivate(aClass.getModifiers()) || Modifier.isProtected(aClass.getModifiers());
    }

    public Set<Class<?>> getAllInterfaces(Set<Class<?>> classes) {
        Set<Class<?>> interfaces = new HashSet<>();

        for (Class<?> c : classes) {
            if (c.isInterface()) {
                interfaces.add(c);
            }
        }
        return interfaces;
    }

    public Set<Package> getAllPackages(Set<Class<?>> classes) {
        Set<Package> packages = new HashSet<>();

        for (Class<?> c : classes) {
            packages.add(c.getPackage());
        }
        return packages;
    }
}
