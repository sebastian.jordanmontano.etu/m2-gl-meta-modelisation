import java.util.HashSet;
import java.util.Set;

public class LinterStructuralRulesChecker {
    private LinterStructuralRules linterStructuralRules = new LinterStructuralRules();
    private Set<Class<?>> classesToCheck = new HashSet<>();

    public LinterStructuralRulesChecker(Set<Class<?>> classesToCheck) {
        this.classesToCheck = classesToCheck;
    }

    public String getErrorMessage(String nameOfTheRule, String classOfMethodName) {
        return "The structural rule: " + nameOfTheRule + " was violated by: " + classOfMethodName;
    }

    public void check() {
        if (!this.linterStructuralRules.allInterfacesShouldBeImplemented(this.classesToCheck)) {
            System.out.println(getErrorMessage("allInterfacesShouldBeImplemented", ""));

        }
        if (!this.linterStructuralRules.packagesShouldHaveAtLeastTwoClasses(this.classesToCheck)) {
            System.out.println(getErrorMessage("packagesShouldHaveAtLeastTwoClasses", ""));

        }

        for (Class<?> c : this.classesToCheck) {
            if (!this.linterStructuralRules.abstractClassShouldHaveAtLeastOneAbstractMethod(c)) {
                System.out
                        .println(getErrorMessage("abstractClassShouldHaveAtLeastOneAbstractMethod", c.getSimpleName()));
            }
            if (!this.linterStructuralRules.enumShouldHaveAtLeastTwoTypes(c)) {
                System.out.println(getErrorMessage("enumShouldHaveAtLeastTwoTypes", c.getSimpleName()));

            }
            if (!this.linterStructuralRules.privateOrProtectedClassesShouldNotHavePublic(c)) {
                System.out.println(getErrorMessage("privateOrProtectedClassesShouldNotHavePublic", c.getSimpleName()));

            }

        }
    }

}
