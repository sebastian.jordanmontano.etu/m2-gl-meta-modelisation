# TP3: Tp noté sur la meta programmation

(!) Dépôt GitLab: https://gitlab.univ-lille.fr/sebastian.jordanmontano.etu/m2-gl-meta-modelisation

* * *

Pour exécuter le code, vous devez d'abord compiler les classes. Dans ce dépôt, les class sont déjà compilées sur le répertoire `bin`.

Pour compiler les classes, faites attention de bien être dans le répertoire de ce TP et pas dans les répertoires des autres. Sinon, il peut avoir problèmes à l'heure de compiler. Moi, pour compiler, j'utilise VisualStudioCode qui a un button qui compile automatiquement.

Après d'avoir compilé les classes, vous devez exécuter le code avec la commande `java App`. Attention que le binarire `App.class` se trouve dedans le répertoire `bin`.

Après, vous aurez un rendu dans le terminal de commande avec le rapport des violations de règles. Par exemple:

![](./img/rendu.png)

Si vous voulez exécuter le code avec des autres classes, vous devez d'abord copier les classes dedans ce projet, compiler à nouveau et changer le code de `App.java`. Maintenant, les règles sont exécutées pour des classes que j'ai créé. Ces classes de test sont dedans le répertoire `testingClasses`. 

Le code à changer est dedans `App.java` ce n'est pas difficile. Vous avez besoin seulement d'instantier la variable `classes`, qui est un ensemble de classes, avec les classes que vous voulez tester.
